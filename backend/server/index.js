/* eslint-disable no-unused-vars */
import { log, table } from 'console';
import express from 'express';
import arangojs from 'arangojs';
import env from './config/config';
import dbConfig from './config/db';

const app = express();
app.use('*', (req, res) => {
  const DB = new arangojs.Database({
    url: dbConfig.url,
  });
  DB.useDatabase(dbConfig.database);
  DB.useBasicAuth(dbConfig.username, dbConfig.password);
  const users = DB.collection('users');
  users.save({ name: 'Elias Bundala', email: 'ebundala@gmail.com' })
    .then((user) => users.all())
    .then((user) => res.json(user))
    .catch((e) => {
      res.json({ error: e.message });
    });
});

app.listen(env.PORT, () => {
  log(`Server listens on port ${env.PORT}`);
});
