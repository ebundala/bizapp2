import env from './config';

const dbConf = {
  url: env.DB_URL, // server url
  database: env.DB_NAME, // db name
  username: env.DB_USER, // db user name
  password: env.DB_PASS, // db password
};
export default dbConf;
